/* -*- indent-tabs-mode: t -*- */

// Copyright (C) 2019-2023 Lawrence Livermore National Security, LLC., Xavier Andrade, Alfredo A. Correa
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <inq/inq.hpp>
#include <string>

int main(int argc, char ** argv){

	using namespace inq;
	using namespace inq::magnitude;

	utils::match energy_match(3.0e-5);

	// set up unit cell 
	auto lParam = 2.108500000000_A;

	systems::ions ions(systems::cell::lattice(
		{0.0_A, lParam, lParam},
		 {lParam, 0.0_A, lParam}, 
		 {lParam, lParam, 0.0_A}));

	// add Mg and O atoms 
	ions.insert_fractional("Mg", {0.0, 0.0, 0.0});
	ions.insert_fractional( "O", {0.5, 0.5, 0.5});
	
	auto nk = 8;
	
	// set up the calculation itself. 
	// nk x nk x nk kpoint mesh 
	// 40 Hartree cutoff
	// boolean: whether or not to apply a 0.5 shift to the kgrid 
	systems::electrons electrons(ions, 
	input::kpoints::grid({nk, nk, nk}, false),
	 options::electrons{}.cutoff(40.0_Ha).extra_states(10));
	

	// xc functional is pbe 
	auto functional = options::theory{}.pbe();
	
	// get the ground state calculation
	// if it was not already available  
	if(not electrons.try_load("mgo_restart")){
		// initial guess seems to happen before every scf 
		ground_state::initial_guess(ions, electrons);
		// first do a coarse SCF calculation
		ground_state::calculate(ions, electrons, options::theory{}.pbe(), inq::options::ground_state{}.energy_tolerance(1e-4_Ha));
		// then do a calculation with a stringent cutoff  
		ground_state::calculate(ions, electrons, functional, inq::options::ground_state{}.energy_tolerance(1e-8_Ha));
		// save to output 
		electrons.save("mgo_restart");
		electrons.save_eigenvalues("mgo_restart");
	}


	/* 
	This is where we define the driving and the time evolution 
	*/

	// define the kick we want to apply 
	auto kick = perturbations::kick{ions.cell(), {0.01, 0.0, 0.0}, perturbations::gauge::velocity};
	//auto kick = perturbations::laser{{1.0, 0.0, 0.0}, 1.0_eV};
	
	// time step and number of times to compute 
	auto const dt = 0.010000;
	long nsteps = 100; //413.41373/dt;
	
	// gpu arrays defined
	gpu::array<double, 1> time(nsteps); // time 
	gpu::array<double, 1> cur(nsteps); // current
	gpu::array<double, 1> en(nsteps);  // total energy 
	gpu::array<double, 1> aind(nsteps); // induced vector potential 

	// set up files for output 
	std::ofstream file;
	if(electrons.root()) { 
		file.open("current.dat");
		// add a header to the file 
		file << "time, current, Ainduced" << std::endl;
	}
	
	// this is a lambda function 
	// this tells the propagate function what to do when running 
	// i.e., what to do with the data at a given time step 
	auto output = [&](auto data){
		
		auto iter = data.iter();
		
		time[iter] = data.time();
		cur[iter] = data.current()[0];
		en[iter] = data.energy().total();
		aind[iter] = data.uniform_vector_potential()[0];
		
		if(data.root()) file << time[iter] << '\t' << cur[iter] << '\t' << aind[iter] << std::endl;
		
		if(data.root() and iter > 1) { // and data.every(N)
			// calculate this on root process 
			// variables: max energy, energy spacing
			// time, time_series
			// this is effectively doing a Fourier transform 
			auto spectrum = observables::spectrum(20.0_eV, 0.01_eV,
			 time({0, iter - 1}), cur({0, iter - 1}));  

			// save spectrum to file 
			std::string fNameSpectrum("spectrum-");
			fNameSpectrum += std::to_string(iter);
			fNameSpectrum += ".dat";
			
			std::ofstream file(fNameSpectrum);
			
			
			for(int ifreq = 0; ifreq < spectrum.size(); ifreq++){
				file << ifreq*in_atomic_units(0.01_eV) << '\t' 
				<< real(spectrum[ifreq]) << '\t' 
				<< imag(spectrum[ifreq]) << std::endl;
			}
		}

		// save wavefunctions for a given time step 
		std::string fNameWfcs("saveWfcs-");
		fNameWfcs += std::to_string(iter);
		
		data.electrons().save_v2(fNameWfcs);
		
		

	};
	
	real_time::propagate<>(ions, // ions as defined
	electrons, // electrons as defined. These are precomputed for ground state from above 
	output, // lambda function telling the code what to do with the data at each time step
	functional.induced_vector_potential(4.0*M_PI), // scaling to updating the vector potential 
	// (in principle we can update the vector potential via TD Maxwell's equations)
	options::real_time{}.num_steps(nsteps).dt(dt*1.0_atomictime).etrs(), // propagation instructions
	// REMOVED: ionic::propagator::fixed{}, // don't move the ions 
	kick); // defined earlier as the applied kick to the system
	
	return energy_match.fail();
	
}


