# 
import numpy as np 
import h5py 
import mpi4py 
from mpi4py import MPI 
import glob 

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# save cpp data to a hdf5 file for each time step 

def progressBar(i,N):
    if rank == 0:
        percentage = int(np.round(i/N*100, decimals = 0))
        numBlocks = 100 
        numBlocksPrint = int(np.round(i/N*numBlocks, decimals = 0))
        numBlocksPrinted = int(np.round((i-1)/N*numBlocks, decimals = 0))
        numBlocksPrintReal = numBlocksPrint - numBlocksPrinted
        
        if numBlocksPrintReal > 0:
            for iBlock in range(numBlocksPrinted,numBlocksPrint):
                value = int(np.round(iBlock/numBlocks*100, decimals = 0))
                print(str(value) + "%", end = ' ')

if rank==0:
    print('MPI comm size:', size)
    
# find out the directories here 
baseName = "saveWfcs-"

# find out the number of time steps to consider 
folders = glob.glob(baseName + "*")
nTimes = len(folders)
nKpts = len(glob.glob(folders[0] + "/kpt-*"))
nBands = len(np.fromfile(folders[0] + "/kpt-0/eigenvalues"))
nG = len(np.fromfile(folders[0] + "/kpt-0/states/0", dtype = np.complex128))

if rank == 0:
    print("nTimes found: " + str(nTimes))
    print("nKpts found: " + str(nKpts))
    print("nBands found: " + str(nBands))
    print("Gvecs found: " + str(nG))

occupationsLoc = np.zeros((nKpts,nBands))
eigenvaluesLoc = np.zeros((nKpts,nBands))
statesLoc = np.zeros((nKpts,nBands,nG), dtype = np.complex128)


# split up tasks among MPI tasks using time steps
# each timestep includes the eigenvalues, occupations, kpoint, and of course wavefunctions
count = -1 
block_size = (nTimes + size - 1 ) // size
for iTime in range(nTimes):
    count += 1 
    if count // block_size != rank:
        continue 
    # otherwise do the hdf5 conversion here 
    fileDir = folders[iTime] #baseName + str(iTime)
    # collect data 
    for iKpt in range(nKpts):
        fileDirK = fileDir + "/kpt-" + str(iKpt)
        occupationsLoc[iKpt,:] = np.fromfile(fileDirK + "/occupations")
        eigenvaluesLoc[iKpt,:] = np.fromfile(fileDirK + "/eigenvalues")
        kptLoc = np.loadtxt(fileDirK + "/kpt.dat")
        # get wavefunctions, too! 
        for iB in range(nBands):
            fileDirB = fileDirK + "/states/" + str(iB)
            statesLoc[iKpt,iB,:] = np.fromfile(fileDirB, dtype = np.complex128)
             
    
    # write this to a HDF5 file 
    fName = fileDir + "/allData.h5"
    
    f = h5py.File(fName, "w")
    f["states"] = statesLoc
    f["occupations"] = occupationsLoc
    f["eigenvalues"] = eigenvaluesLoc 
    f["kpt"] = kptLoc 
    
    
    f.close()
    progressBar(count,block_size)
    
if rank == 0:
    print("done!")

