import numpy as np
import h5py 

nTimes = 100 

# first, get size of array needed
baseName = "spectrum-" 
omega = np.loadtxt(baseName + "2.dat", usecols = 0)
nOmega = omega.shape[0]

saveSpectra = np.zeros((nTimes,nOmega), dtype = np.complex128)

for iTime in range(2,nTimes):
    fileName = baseName + str(iTime) + ".dat"
    saveSpectra[iTime,:] = np.loadtxt(fileName, usecols = (1,2)).view(np.complex128).flatten()

f = h5py.File("allSpectra.h5", "w")
f["spectra"] = saveSpectra 
f["omega"] = omega 

f.close()